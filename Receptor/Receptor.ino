/**** CONEXÕES MODULO PARA ARDUINO ****

NRF  |  ARDUINO
Vcc  => + 3.3v
GND  => GND
CE   => PIN 9 
CSN  => PIN 10 - SS
SCK  => PIN 13 - SCK
MO   => PIN 11 - MOSI
MI   => PIN 12 - MISO
IRQ  => SEM USO

SDcard  |  ARDUINO
MOSI    => pin 11
MISO    => pin 12
CLK     => pin 13
CS      => pin 4

*/

/** Bibliotecas **/
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <SD.h>

File myFile;
 
double analogico[2]; // Matriz do tipo int chamada analogico

RF24 radio(7,8); // Cria um objeto chama radio conectado nos pinos 9 e 10 do Arduino

String fn = "test.txt";
char fileName[sizeof(fn)+2];

const int sd_cs = 4;
const int nrf_cs = 8;
                  
/* Configura o endereço de comunicação entre os Módulos */                  
const uint64_t pipe = 0xE8E8F0F0E255;//LL; // Tubo de comunicação

void setup(void)
{
  fn.toCharArray(fileName, sizeof(fileName)); //apenas para definir o valor de fileName
  
  Serial.begin(9600);            // Inicia a Serial
  Serial.println("Sou o receptor\n\n");
  
  pinMode(sd_cs, OUTPUT); // Set pino como saida
  pinMode(nrf_cs, OUTPUT); // Set pino como saida
  pinMode(10, OUTPUT);
  
  ativaModulo("sd");
   
  if (!SD.begin(sd_cs)) {
    Serial.println("initialization SD failed! \n");
    return;
  }
  Serial.println("initialization SD DONE. \n");

  if (SD.exists(fileName)) {
    Serial.print(fileName);
    Serial.println(" exists. \n\n");
  }
  else {
    Serial.println(fileName);
    Serial.println(" doesn't exist. \n\n");
  }
  
    ativaModulo("nrf");
    
    radio.begin();                 // Inicia o modulo para comunicação.

    radio.setChannel(5);           // Set canal de comunicacao
  
    radio.openReadingPipe(1,pipe); // abrindo o meio de comunicação (Tubo), com
                                 // o endeço definido no ínicio do programa
    radio.startListening();        // Inicia o modulo para ouvir as requisições.
    
    Serial.println("\n\n\n\n\n Escutando... \n\n\n\n\n");
}

void loop()
{
  //digitalWrite(nrf_cs,LOW);
    //digitalWrite(8,HIGH);
  //ativaModulo("nrf");
  delay(5); // Delay para atualização
  /* verifica se o radio ouviu */
  if ( radio.available() )
  {
    boolean done = false;
    boolean done0 = false;
    analogico[0] = 0.01;
    while (!done)
    {
      done0 = radio.read( analogico, sizeof(analogico) );
      if(analogico[0]!=0.01 && done0)
        done = true;
    }
    
      /* Mostra na Serial o Valor de A0 e A1 enviado pelo trasmissor */
    Serial.print("Receptor - A0 Recebido:  ");
    Serial.print(analogico[0]);
    Serial.print("  ");
    Serial.print("  A1 recebido:  ");
    Serial.println(analogico[1]);
    Serial.println("");
    
    radio.stopListening();
    ativaModulo("sd");
  
    myFile = SD.open(fileName, FILE_WRITE);
    if (myFile) {
      Serial.println("Open File!!! \n");
    
      myFile.print("Receptor - A0 Recebido:  ");
      myFile.print(analogico[0]);
      myFile.print("  ");
      myFile.print("  A1 recebido:  ");
      myFile.println(analogico[1]);
      myFile.println("");
    
      myFile.close();
    } else {
      // if the file didn't open, print an error:
      Serial.println("Error opening!!! \n");
    }
    
    ativaModulo("nrf");
    radio.startListening();
    
  }
  else
  {
    Serial.println("Nenhum dado recebido");
  }
}

void ativaModulo(String modulo){
  if(modulo == "sd"){
    digitalWrite(nrf_cs,HIGH);   //desliga nrf
    digitalWrite(sd_cs,LOW);     //liga sd
    Serial.println("sd ativado \n\n");
  }else if(modulo == "nrf"){
    digitalWrite(sd_cs,HIGH);    //desliga sd
    digitalWrite(nrf_cs,LOW);    //liga nrf
    Serial.println("nrf ativado \n\n");
  }
}
