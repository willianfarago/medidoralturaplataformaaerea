/*  
***** TRANSMISSOR *****

Aplicação: Este programa envia duas variáveis inteiras
           recebidas em duas entradas analógicas 
           e envia via rádio usando um módulo nRF24L01+
           

NRF  |  ARDUINO
Vcc  => + 3.3v
GND  => GND
CE   => PIN 9 
CSN  => PIN 10 - SS
SCK  => PIN 13 - SCK
MO   => PIN 11 - MOSI
MI   => PIN 12 - MISO
IRQ  => SEM USO
*/

/* Bibliotecas */
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include <SFE_BMP180.h>
#include <Wire.h>

double analogico[2]; // Matriz do tipo int chamada analogico

SFE_BMP180 pressure;

double baseline; // baseline pressure

const int repetitions = 20;

//Endereco I2C do MPU6050
const int MPU=0x68;

//Variaveis para armazenar valores dos sensores
int AcX, AcX, AcX;
//Constantes de inicio
int initAcX, initAcX, initAcX;

RF24 radio(9,10);  // Cria um objeto chama radio conectado nos pinos 9 e 10 do Arduino

const uint64_t pipe = 0xE8E8F0F0E255;//LL; //Configurando endereço de comunicação entre os MÓDULOS,

void setup(void)
{
  Serial.begin(9600);          // Inicia a Serial
  
  // Initialize the sensor (it is important to get calibration values stored on the device).
  if(pressure.begin())
    Serial.println("BMP180 init success");
  else{
    Serial.println("BMP180 init fail (disconnected?)\n\n");
    while(1); // Pause forever.
  }
    
  // Get the baseline pressure:
  delay(500);
  for(int i=0; i<(repetitions*2); i++)
    baseline += getPressure();
  baseline = baseline/initBase;
  
  Serial.print("baseline pressure: ");
  Serial.print(baseline);
  Serial.println(" mb");  
  
  radio.begin();               // Inicia o modulo para comunicação.
  
  radio.setChannel(5);         // Set canal de comunicacao
  
  radio.openWritingPipe(pipe); // abrindo o meio de comunicação (Tubo), com
                               // o endeço definido no ínicio do programa
  
  //radio.setDataRate(RF24_250KBPS);
  //radio.setPALevel(RF24_PA_MAX);
}

void loop()
{
  //analogico[0] = analogRead(A6); // Faz a leitura analogica no pino A0
  //analogico[1] = analogRead(A7); // Faz a leitura analogica no pino A1

  double a,P;
  
  // Get a new pressure reading:
  P = 0;
  for(int i=0; i<repetitions; i++)
    P += getPressure();
  P = P/repetitions;

  // Show the relative altitude difference between
  // the new reading and the baseline reading:

  a = pressure.altitude(P,baseline);
  
  analogico[0] = a;
  analogico[1] = 0.0;

  radio.write(analogico, sizeof(analogico)); // Envia a Matriz analogico

  delay(5);
}

////////////////////////////////FIM loop//////////////////////////////

double getPressure()
{
  char status;
  double T,P,p0,a;

  // You must first get a temperature measurement to perform a pressure reading.
  
  // Start a temperature measurement:
  // If request is successful, the number of ms to wait is returned.
  // If request is unsuccessful, 0 is returned.

  status = pressure.startTemperature();
  if (status != 0)
  {
    // Wait for the measurement to complete:

    delay(status);

    // Retrieve the completed temperature measurement:
    // Note that the measurement is stored in the variable T.
    // Use '&T' to provide the address of T to the function.
    // Function returns 1 if successful, 0 if failure.

    status = pressure.getTemperature(T);
    if (status != 0)
    {
      // Start a pressure measurement:
      // The parameter is the oversampling setting, from 0 to 3 (highest res, longest wait).
      // If request is successful, the number of ms to wait is returned.
      // If request is unsuccessful, 0 is returned.

      status = pressure.startPressure(3);
      if (status != 0)
      {
        // Wait for the measurement to complete:
        delay(status);

        // Retrieve the completed pressure measurement:
        // Note that the measurement is stored in the variable P.
        // Use '&P' to provide the address of P.
        // Note also that the function requires the previous temperature measurement (T).
        // (If temperature is stable, you can do one temperature measurement for a number of pressure measurements.)
        // Function returns 1 if successful, 0 if failure.

        status = pressure.getPressure(P,T);
        if (status != 0)
        {
          return(P);
        }
        else Serial.println("error retrieving pressure measurement\n");
      }
      else Serial.println("error starting pressure measurement\n");
    }
    else Serial.println("error retrieving temperature measurement\n");
  }
  else Serial.println("error starting temperature measurement\n");
}

